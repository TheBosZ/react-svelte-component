# react-svelte-component

> A component to use Svelte components in your React app

[![NPM](https://img.shields.io/npm/v/react-svelte-component.svg)](https://www.npmjs.com/package/react-svelte-component)

## Install

```bash
npm install --save react-svelte-component
```

## Usage

```tsx
import * as React from 'react'

import SvelteComponent from 'react-svelte-component'
import SvelteThing from './SvelteThing.svelte';

class Example extends React.Component {
  render () {
    return (
      <SvelteComponent this={SvelteThing} />
    )
  }
}
```

## Thanks
Based on the [react-svelte](https://github.com/Rich-Harris/react-svelte/) library by Rich Harris. Thanks!

## License

MIT © [TheBosZ](https://github.com/TheBosZ)
