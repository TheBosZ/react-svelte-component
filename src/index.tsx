/**
 * @class SvelteComponent
 */
import * as React from 'react';

export type MyProps = {
	this: any;
	component?: React.ElementType | string;
	[name: string]: any;
};

export type SvelteInterface = {
	$set: (props: any) => void;
	$destroy: () => void;
};

export default class SvelteComponent extends React.Component<MyProps, {}> {
	private readonly container: React.RefObject<any>;
	private instance: SvelteInterface | null;
	private readonly fragment: React.ReactElement<React.ReactFragment>;

	constructor(props: MyProps) {
		super(props);

		this.container = React.createRef();
		this.instance = null;
		let {component} = props;
		if (!component) {
			component = 'span';
		}
		this.fragment = React.createElement(component, {ref: this.container});
	}

	componentDidMount() {
		const {this: Constructor, ...props} = this.props;

		this.instance = new Constructor({
			target: this.container.current,
			props
		});
	}

	componentDidUpdate() {
		if (this.instance) {
			this.instance.$set(this.props);
		}
	}

	componentWillUnmount() {
		if (this.instance) {
			this.instance.$destroy();
		}
	}

	render() {
		return this.fragment;
	}
}
